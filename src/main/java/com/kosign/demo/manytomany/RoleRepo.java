package com.kosign.demo.manytomany;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepo extends JpaRepository<Role,Long> {

    @Query(nativeQuery = true,value = "select * from Role  where Role .id = :id")
    Role customQuery(Long id);
}
