package com.kosign.demo.manytomany;

import javafx.util.Builder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("/users/create")
    public ResponseEntity createUser(@RequestBody User user){
        return ResponseEntity.ok(userService.createUser(user));
    }

    @GetMapping("/users")
    public List<User> findAll(){
        return userService.findAll();
    }

    @GetMapping("/users/{id}")
    public User findUserById(@PathVariable Long id){
        return userService.findById(id);
    }

    @DeleteMapping("/users/{id}")
    public Object deleteUser(@PathVariable Long id){
        User user = userService.findById(id);
        System.out.println(user.toString());
        userService.deleteUser(id);
        return user;
    }

    @Autowired
    RoleRepo roleRepo;

    @PostMapping("/roles/create")
    public Object createRole(@RequestBody Role role){
        return roleRepo.save(role);
    }

    @GetMapping("/roles")
    public List<Role> Roles(){
        return roleRepo.findAll();
    }

    @GetMapping("/roles/{id}")
    public Role getRoleById(@PathVariable Long id){
        //return new ResponseEntity(roleRepo.customQuery(id).getUsers(),HttpStatus.OK);
        return roleRepo.findById(id).orElse(new Role());
    }

    @DeleteMapping("/roles/{id}")
    public Object deleteRoleById(@PathVariable Long id){
        Role role = roleRepo.findById(id).orElseThrow(()-> new RuntimeException());
        roleRepo.delete(role);
        return role;
    }

    @PutMapping("/roles/{id}")
    public Object updateRoles(@PathVariable Long id,@RequestBody Role payload){
        Role role = roleRepo.findById(id).orElseThrow(()-> new RuntimeException());
        role.setName(payload.getName());
       return roleRepo.save(role);
    }
}
