package com.kosign.demo.manytomany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity(name = "users")
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;

    @Column(unique = true)
    private String email;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ManyToMany(cascade = CascadeType.DETACH,fetch = FetchType.LAZY)
    private List<Role> roles;

}
